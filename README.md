Welcome !

The **Photons and Neutrons Team** (aka PaN) aims at fostering any good-willing initiative to promote scientific software for photon and neutron scattering research. 
[Debian](https://www.debian.org/) is a major :penguin: Linux distribution at the root of many other flavours (including [Ubuntu](https://ubuntu.com/), [Mint](https://www.linuxmint.com/)).
It is specifically suited for science with many available scientific packages.
Having Debian packages helps a lot :+1: in deployment and maintenance over time. 

This team, and its projects, complement the more global effort of the [Debian Science Team](https://wiki.debian.org/DebianScience/) and its [Salsa project](https://salsa.debian.org/science-team).
You can also have a look at the [Pan-Data software catalog](https://software.pan-data.eu/). We use the [pan-maintainers](https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-pan-maintainers) mailing list for our exchanges.

If you know about good software, and wish to promote it through Debian, please join our group and projects.
- If you are working in a photon/neutron facility, you are welcome to add your facility logo and link in the table below, and write a dedicated page about your efforts, preferred software, etc. 
- If you are not part of a facility, but are a user of these, please add your wishes to the [Users Packaging](https://salsa.debian.org/pan-team/users-packaging) project.

Affiliated facilities
---
|[**Synchrotron Soleil**](https://www.synchrotron-soleil.fr/en)|
|---|
|![Synchrotron SOLEIL](https://www.synchrotron-soleil.fr/sites/default/files/logo_0.png) |
| [Our Debian-PaN-Soleil page](https://salsa.debian.org/pan-team/soleil-packaging-overview) |

Current international collaborations
---

|[PaNOSC](https://www.panosc.eu/)|[ExPaNDS](https://expands.eu/)|[LEAPS](https://leaps-initiative.eu/)|
|---|---|---|
|![PaNOSC](https://www.panosc.eu/wp-content/uploads/2019/04/logo.svg) |![ExPaNDS](https://i0.wp.com/expands.eu/wp-content/uploads/2019/09/Expands_text_header.png?resize=200%2C25&ssl=1) |![LEAPS](https://leaps-initiative.eu/wp-content/uploads/2019/11/cropped-LEAPS_logo_colour-e1573546128283.jpg)|

Past international collaborations
---

| [PanData](http://pan-data.eu/) | [Calipso+](http://www.calipsoplus.eu/) | 
|---|---|
|![PanData](https://software.pan-data.eu/bundles/app/images/pandata-logo.png) |![Capilso+](http://www.calipsoplus.eu/wp-content/uploads/2017/08/HH_Logo_Calipsoplus_RGB_pos.jpg)|

Related Debian resources
---

Most of the photons and neutrons packages are also mentioned as [Tasks in the DebianPAN Blend](https://blends.debian.org/pan/tasks/index) and its automatic [status page](https://salsa.debian.org/blends-team/pan). 
A 'Blend' is a collection of *metapackages* i.e. prepared sets of Debian packages.

Most Blends are listed at https://salsa.debian.org/blends-team, especially the:
- **PaN** https://salsa.debian.org/blends-team/pan
- **Science** https://salsa.debian.org/blends-team/science and https://salsa.debian.org/science-team
- **Bio-Linux** https://salsa.debian.org/blends-team/bio-linux
- **DebiChem** https://salsa.debian.org/blends-team/debichem
- **Med** https://salsa.debian.org/blends-team/med
- **Imaging** https://salsa.debian.org/blends-team/imaging
- **Astro** https://salsa.debian.org/debian-astro-team/debian-astro

Science packaging for Debian
---
We encourage you to take part in any packaging initiative, through the [PaN Blend](https://salsa.debian.org/blends-team/pan) and [Debian Science](https://salsa.debian.org/science-team). 
There are many existing documentation about the procedure to follow to import the 'upstream' source code, 
and add Debian information (license, patches, dependencies, ...) to create a source package, for instance:
- https://wiki.debian.org/Python/GitPackaging
- http://marquiz.github.io/git-buildpackage-rpm/gbp.import.html
- https://wiki.debian.org/DanielKahnGillmor/preferred_packaging

:warning: Before starting a new packaging on Salsa, make sure nothing has been done before in this regard (look at the [Science team](https://salsa.debian.org/science-team) ), or contact authors from previous attempts.
